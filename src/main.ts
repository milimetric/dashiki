import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'

// TODO: check this is how we're supposed to import style,
//   I assumed it would come with each component on import
import '@wikimedia/codex/dist/codex.style.css'

const app = createApp(App)

app.use(createPinia())
app.use(router)

app.mount('#app')
